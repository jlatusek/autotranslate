#!/usr/bin/env python3
import argparse
import os
import time

import deepl
import gi
from google.cloud import translate

gi.require_version("Gtk", "4.0")
gi.require_version("Gdk", "4.0")
from gi.repository import Gdk, Gtk, GLib, GObject

CHECK_DELAY = 0.3

class Translator:
    def translate(self, text: str, target: str) -> str:
        raise NotImplementedError


class DeeplTranslator:
    def __init__(self):
        auth_key = os.getenv("DEEPL_APP")
        self._translator = deepl.Translator(auth_key)

    def translate(self, text: str, target="pl") -> str:
        return self._translator.translate_text(text, target_lang=target)


class GoogleTranslator:
    def __init__(self):
        self._client = translate.TranslationServiceClient()
        self._project_id = "fourth-jigsaw-408813"
        self._location = "global"
        self._parent = f"projects/{self._project_id}/locations/{self._location}"

    def translate(self, text: str, target="pl") -> str:
        response = self._client.translate_text(
            parent=self._parent,
            contents=[text],
            mime_type="text/plain",
            source_language_code="en-US",
            target_language_code="pl",
        )
        return response.translations[0].translated_text


class MessageWindow(Gtk.Window):
    def __init__(self, text_original, text_translated):
        super().__init__(title="Translation")
        self.label = Gtk.Label(label=text_original)
        self.button = Gtk.Button(label="Ok")
        self.text_translated = Gtk.TextView()
        text_buffer = Gtk.TextBuffer()
        text_buffer.set_text(text_translated)
        self.text_translated.set_buffer(text_buffer)

        self.button.connect("clicked", self.on_button_clicked)

        self.set_default_size(150, 200)

        box = Gtk.VBox(spacing=6)
        box.add(self.label)
        box.add(self.text_translated)
        box.add(self.button)
        self.add(box)

        self.show_all()

    def on_button_clicked(self, widget):
        self.destroy()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-e",
        "--engine",
        choices=["google", "deepl"],
        default="google",
        help="Translation engine",
    )
    parser.add_argument(
        "-sc",
        "--set-clipboard",
        action="store_true",
        default=False,
        help="Set clipboard to translated text",
    )
    return parser.parse_args()


class AutoTranslator:
    def __init__(self):
        self._clipboard = Gdk.Display().get_default().get_clipboard()
        self._translator = None
        self._set_clipboard = False
        self._recent_clipboard_text = ""

    def clipboard_update_cb(self, clipboard, result, user_data):
        try:
            text = self._clipboard.read_text_finish(result)
        except Exception as e:
            print(e)
            GLib.timeout_add_seconds(CHECK_DELAY, self.read_clipboard)
            return
        if not text or text == self._recent_clipboard_text:
            GLib.timeout_add_seconds(CHECK_DELAY, self.read_clipboard)
            return
        translation = self._translator.translate(text)
        self._recent_clipboard_text = translation
        print(translation)
        if self._set_clipboard and isinstance(translation, str):
            text_bytes = GLib.Bytes.new(translation.encode("utf-8"))
            provider = Gdk.ContentProvider.new_for_bytes("text/plain;charset=utf-8", text_bytes)
            ret = self._clipboard.set_content(provider)
            if ret:
                print("Clipboard set")
            else:
                print("Clipboard not set")
        GLib.timeout_add_seconds(CHECK_DELAY, self.read_clipboard)

    def on_activate(self, app):
        win = Gtk.ApplicationWindow(application=app)
        win.present()

    def read_clipboard(self):
        self._clipboard.read_text_async(cancellable=None, callback=self.clipboard_update_cb, user_data=None)

    def main(self):
        args = parse_args()
        self._set_clipboard = args.set_clipboard
        if args.engine == "google":
            self._translator = GoogleTranslator()
        elif args.engine == "deepl":
            self._translator = DeeplTranslator()
        self.read_clipboard()
        app = Gtk.Application()
        app.connect("activate", self.on_activate)
        app.run(None)


if __name__ == "__main__":
    AutoTranslator().main()
